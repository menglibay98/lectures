<?php
//Task 1

function is_negative($numbers)
{
    if($numbers < 0) {
        return "Отрицательное число";
    } elseif($numbers === 0) {
        return "Это Нулевое число";
    } else {
        return "Это не отрицательное число число";
    }
}

$numbers = -5;
$second_number = 3;
$zero_number = 0;

echo is_negative($numbers) . PHP_EOL;
echo is_negative($second_number) . PHP_EOL;
echo is_negative($zero_number) . PHP_EOL;


//Task 2

function length_string($string)
{
    return strlen($string);
}

$bestStudent = "Zamanbek Sadyrbek";
$normStudent = "Beibarys Kurmanbayev";
$myFavStudent = "Dmitriy Tonkonog";
echo length_string($bestStudent) . PHP_EOL;
echo length_string($normStudent) . PHP_EOL;
echo length_string($myFavStudent) . PHP_EOL;

//Task 3
function hasLetterA($string) {
    if(strpos($string,'a') !== false) {
        return "Да есть";
    } else {
        return "Нету такого";
    }
}

$test_x = "Qaraqat";
$text_b = "Nurly";

echo hasLetterA($test_x) . PHP_EOL;
echo hasLetterA($text_b) . PHP_EOL;

// Task 4
function checkDivisible($number) {
    switch ($number){
        case ($number % 2 == 0):
            echo "Число делится на 2" . PHP_EOL;
            break;
        case ($number % 3 == 0):
            echo "Число делится на 3" . PHP_EOL;
            break;
        case ($number % 5 == 0):
            echo "Число делится на 5" . PHP_EOL;
            break;
        case ($number % 6 == 0):
            echo "Число делится на 6" . PHP_EOL;
            break;
        case ($number % 9 == 0):
            echo "Число делится на 9" . PHP_EOL;
            break;
        default:
            echo "Число не делится" . PHP_EOL;
    }
}

checkDivisible(45);
checkDivisible(67);
checkDivisible(81);

//Task 5

function checkDivisible2($number) {
    switch ($number){
        case ($number % 3 == 0):
            echo "Число делится на 3" . PHP_EOL;
            break;
        case ($number % 5 == 0):
            echo "Число делится на 5" . PHP_EOL;
            break;
        case ($number % 7 == 0):
            echo "Число делится на 7" . PHP_EOL;
            break;
        case ($number % 11 == 0):
            echo "Число делится на 11" . PHP_EOL;
            break;
        default:
            echo "Число не делится" . PHP_EOL;
    }
}

checkDivisible2(9);
checkDivisible2(25);
checkDivisible2(49);
checkDivisible2(44);


//Task 6 & 7
function lastSymbol($string){
    return substr($string,-1);
}

$random = "Alinur";
$random2 = "Sanzhar ";
$random3 = "Kravchenkko Denis";
echo lastSymbol($random) . PHP_EOL;
echo lastSymbol($random2) . PHP_EOL;
echo lastSymbol($random3) . PHP_EOL;


//Task 8
function triangleArea($a,$b,$c){
    $p = ($a + $b + $c) / 2;

    $area = sqrt($p * ($p - $a) * ($p - $b) * ($p - $c));

    echo "Площадь треугольника: " . $area . PHP_EOL;
}

triangleArea(3,5,6);
triangleArea(36,34,54);


// Task 9
function rectangleArea($a,$b) {
    $area = $a * $b;

    echo "Площадь прямоугльника: " . $area . PHP_EOL;
}

rectangleArea(4,5);
rectangleArea(4,2);


//Task 10
function quadrad($number){
    $square = 0;
    for($i = 0; $i < $number;$i++){
        $square += $number;
    }

    echo $square . PHP_EOL;
}

quadrad(6);
quadrad(8);